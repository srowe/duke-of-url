# duke-of-url

## What is this thing?

A hack. Make no mistake. This is just some fun and you shouldn't take it seriously or expect it to be bulletproof in any capacity.

There are, in the world, these self-hosted dashboard/launcher things that are, for my needs, far too limited/rigid in what they accomplish. So this is kinda an old school version of that idea. If you prefer typing to clicking, you might dig it.

The main interface is a text prompt where you can dump URLs, set up aliases to search engines and more. If you use multiple browsers on multiple operating systems, Duke can be pretty handy because it de-couples your data from a browser instance.

If, as you read this, you think to yourself "but my browser can do all of this stuff!" You're correct. If you ever switch browsers or if you frequently use multiple browsers, you can set all of these things up in each browser and risk upgrades modifying/removing them or you can outsource them to Duke. Then all you need to do is visit Duke, right-click on his URL and add it to your new browser.

[[_TOC_]]

### Features

- Browser-independent infinite history of data passing through it
  - History searching
  - Bash-like history substitution
- Privacy focused with no external dependencies
- Shell-like Aliases with or without arguments
  - With arguments for things like search engines
  - Without arguments for quick access to frequently visited sites
  - Matchers that use the power of regular expressions and computers to match any incoming text to send you to the right place (think issue-tracker names.)
- Bookmarking with folksonomy (tagging)
  - Bookmark searching
  - View bookmarks by tag union
- Tab completion of almost everything in a contextually-aware way
- For some reason there's a Home Assistant integration
- Easily extensible with plugins in your favorite language: Ruby
- Support for OpenSearch
  - This means you can easily set up Duke as a/the search engine for your browser
- Some UI customization
- Basic math!
- Works out of the box with Sqlite and Postgresql

#### Things it might do someday

- Have a fancier UI
  - Maybe with plugins to track your Plex viewers or whatever those other dashboard dealies do
  - More customization beyond the current scope
- More cool things

#### Things it probably won't do
- Have authentication
- Be suitable to be visible on the Internet
- Catch on


# Install

## Docker

### with SQLite

```bash
mkdir duke
docker run --rm -dv ${PWD}/duke:/app/userdata -p 4567:4567 --name dukeofurl registry.gitlab.com/srowe/duke-of-url:latest
```

### with Postgres

This is the past least travelled, FYI. SQlite is plenty fast and has the most users.

```bash
# ensure your Postgres host has a database for Duke
docker run --rm -d -e DUKE_DB="postgres://pguser:pgpass@pghost:5432/dukeofurl" -p 4567:4567 --name dukeofurl registry.gitlab.com/srowe/duke-of-url:latest
```

### With Docker-compose

```docker
version: '3.5'

services:
  dukeofurl:
    container_name: dukeofurl
    image: registry.gitlab.com/srowe/duke-of-url:latest
    # delete the Environment section if using SQlite
    environment:
      DUKE_DB: postgres://pguser:pgpass@pghost:5432/dukeofurl
    # delete the Volumes section if using PG
    volumes:
       - ./userdata:/app/userdata
    ports:
      - "4567:4567"
    networks:
      - dukeofurl
    restart: unless-stopped

networks:
  dukeofurl:
    driver: bridge
```

For the above to work, you need to remove either the `environment:` section or the `volumes:` section. If you're passing in a volume, the directory needs to exist.

## Manual
clone the repo

Have the deps for the gems installed. If you've chosen this route, I'll assume you know waht you're doing. For Debian you'll need `libpq-dev` because calling it libpg would be too obvious, I guess.

By default data will be stored in `sqlite://userdata/duke.db`. If you want to store it somewhere else via sqlite set the environment variable `DUKE_DB` to the URL of your choosing. Duke of Url has also been tested with Postgres 15.3 with a URL like `postgres://user:pass@host:5432/duke`.

Older versions of PostgreSQL are sure to work as nothing fancy is going on. Just ensure that the database exists and the user has the usual permissions.

```bash
cd app/
bundle config set --local path vendor/bundle
bundle install --binstubs
mkdir userdata
```

running in dev mode
```bash
bin/rerun -- bin/rackup -o 0
```

Production mode not yet supported, but it's something like this:
```bash
bin/rackup -o 0
```

# Integrate with your browser

## Firefox/Librewolf
If you right-click in the URL bar, you can add Duke as a search engine and then in your browser go to about:preferences click Search and set Duke to the default search engine.

You might also want this [Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/new-tab-override/) which allows you to have Duke (or any other URL) auto-load with new tabs.

## Other browsers
Submit a PR with instructions, please!

### If you don't want to commit
If you right-click the text input, you can click "Add a Keyword for this search" so that you can use Duke directly from your URL bar. If you relegate Duke to living behind a "keyword," you'll need to preface the commands below with that keyword if you're entering them into the browser's URL bar. Hopefully that's obvious.

You might also want to set Duke as the default location for new windows.


# Input
There's tab-completion for stuff. If you have a partially-completed input, you can hit ctrl-d to see completables. The tab completion can optionally show you usage statements for most commands. You can also use up-arrow to put recent history into the input box.

Right now, hitting return on a partially-completed input doesn't complete the input. Seems tricky either way.

Start by defining an Alias or two.


## Aliases
You type:

`alias dd https://duckduckgo.com/?t=ffab&q=${*}` then you can `dd my search terms`
or
`alias wikipedia https://en.wikipedia.org/w/index.php?fulltext=1&search=${*}&title=Special%3ASearch&ns0=1` then you can `wikipedia guild wars 2`

The magic is the `${*}` where the search terms go. To add more, go to your site and search for `example` and then replace `example` with `${*}` as your alias template and you're set.

Positional parameters aren't yet supported and likely won't be w/o a good reason.

If you're on a site and wish to stay there, while also creating an alias for it, you can `aliasr whatever https://wherever.com`. This works best when Duke is either your default search engine or when used with a search engine keyword as described in "Integrate with your browser."

You can alias Duke commands without recursion. Multi-commands (below) can be aliases and consist of calls to aliases as long as the alias doesn't recurse.

`aliases` allows you to browse your aliases.

`unalias wikipedia` also works.

You can also alias "mundane" URLs like `alias supermicro-ipmi http://10.99.99.12/`.

You can redefine an alias by setting the same alias again. To assist with this, you can alt-click (ctrl+alt+click for Linux users for some reason) an alias in the UI to bring its definition into the main form.

### Matchers
Matchers are more advanced, but if you know what a regular expression capture group is, you'll do just fine.

Aliases are just a helper around Matchers. Matchers try to *match your input to their regular expression* and if there's a match, make a love connection. So `alias dd https://duckduckgo.com/?t=ffab&q=${*}` is really just a Matcher with `^dd\s+(.*)` as the regular expression. The other example of `alias supermicro-ipmi http://10.99.99.12/` is really a Matcher with the regular expression of `^supermicro-ipmi$`. Aliases require you to use their keyword, matchers do not. Matchers just match input.

Due to the trickiness of trying to decide where your regex ends, there is instead a gui Matcher editor that you can get to with `edit matcher <MATCHER_LABEL>`. Matchers require labels to make it easier to find them when you want to edit them.

Matcher example use case: Let's say your issue-tracking system creates issues that look like HDMI-1234 and/or DISPLAYPORT-3214. You can create a matcher with a regex like: `^([a-z]+-\d+)$` set to case-insensitive. Set your destination to `https://your.probablyhorribletickettracker.com/browse/${*}` and now you can just dump ticket names directly into Duke and you'll go to the right place. You could also do this with *regular* shipping carrier tracking numbers, etc. Do note that setting a regex this loose might trap things you didn't intend and then you'll be gnawing your own leg off to escape. So perhaps a better suggestion would be `^((?:HDMI|DISPLAYPORT)-\d+)$` in case you ever want to use the default alias to search for the new `somedevice-5000`.

Matchers can also use positional arguments. If your matcher looks like this: `^(mything|myotherthing) (volume|brightness) (\d+)$` and your template looked like this `https://example.app/?object=${1}&control=${2}&level=${3}` you could type `mything brightness 22` or `myotherthing volume 99` etc. The positional paramteters on the destination side can of course be in any order and will match their number to the capture group's index(-1 when starting at 0).

Matcher with positional args example/far-fetched use case: stripping tracking/referral variables from a known destination's URLs. Matcher: `(https://somesite.com/?.*)&EVILTRACKER=[^&]+(.*)` Template: `${1}${2}`. Ya know... whatever.

You can, of course, nest capture groups. The positional argument stuff isn't a hot path in my usage, so YMMV when it comes to doing complex shenaniganz.

Hey speaking of complexity, if the javascript-based tester provides different results than Duke itself, please file a bug. Also, there's an undocumented(?) command called `debug` that can show you what Duke thinks about any input, including input matched by a Matcher. Using the example from above, you could type `debug HDMI-2222` to see if Duke matched that input to the Matcher you intended and then what that Matcher found within itself. `debug` works on any Duke input.


## History expansion currently implemented:
1. !$
1. !*
1. !?<needle>
1. !!
1. !n

If you're not an intermediate+ Bash user, this might just be gibberish to you. Just like in Bash, these features aren't required. Feel free to skip over them and Duke will still be useful.

You can also type `history` and `history? searchTerm(s)`, the latter of which will do a `%searchTerm(s)%` 'LIKE' SQL query against your history.

If your input has a leading space, it will not go into history (ala HISTCONTROL=ignorespace in bash). Although it's kinda more like HISTCONTROL=ignoreboth because history is de-duped already.

If you want to bookmark a history entry using `!!`, and that history entry has spaces (for example you want to bookmark your favorite search), you need to quote `!!` like so: `bm "!!" bash, significant, whitespace`

### EMACSish keybinding
Lacking a better place to put this information in this README, you can use ctrl-u, while in the text input, to clear the text input. This is vaguely an EMACS keybinding. I was going to implement ctrl-w and ctrl-k as well, but the browsers really want those keybindings and I can only climb so high.


## Multiple commands at once
You can issue multiple commands at once by separating them with ` && ` (note the spaces). Only the final "command" can be one that redirects you somewhere.

This is mostly useful for setting several preferences at once, but could be used to make the HASS integration even dumber. The poorest-man's automation.

### Examples
* `ha toggle light.my_entity && 111111111 * 111111111` You don't ever turn on a light and then do some math?
* `alias darkmode set ui:link:color white && set ui:background linear-gradient(132deg, #536976 0.00%, #292E49 100.00%);`
* `alias rootytootyfreshnfruity set ui:link:color Gold && set ui:background linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%) && set ui:button:color Gold`

If the above looks bonkers, you can also set your UI prefs one at a time and then use the `saveui` command described below.


## Bookmarks
In short:

`bm https://zombo.com philosophy comedy old-school`

`bm !! history expansion awesome`

`bmr https://www.proxmox.com qemu, virtualization, debian` Similar to `aliasr`, create a bookmark and _return_ to the URL.

`bmt comedy`  Show bookmarks browser with bookmarks tagged `comedy`.

`bmt comedy old-school`  Show bookmarks browser with bookmarks tagged `comedy` AND `old-school`.

`bms` Show bookmarks browser

`bm? search string` Search bookmarks for `search string`; this is a dumb SQL LIKE query.

`bmretag old-tag new-tag` Rename a tag.

Tags are space separated. Setting a bookmark again overwrites its tags.

You can quickly `bmretag` by alt-clicking (ctrl+alt+click for Linux users for some reason) a tag in the UI. This also works for editing bookmarks; just alt-click the destination.

## & and &- commands
Sometimes one finds oneself on a page that they don't necessarily want to bookmark, but they wouldn't mind having in history; perhaps for viewing on a different device at a later time. To get this URL into history one could, of course, do this:

1. ^l
1. ^c
1. alt+Home
1. ^v
1. Enter

That's a lot. The `&` command drastically and dramatically shortens this process to:

1. ^l
1. Home (or however you get to start of line on your OS/browser)
1. &
1. Space
1. Enter

Oh, it's the same amount of steps... but fewer [double-buckys](https://www.hackerposse.com/jargon/html/D/double-bucky.html) (buckies?) and maybe less cognitive load... for me anyway. If this is too confusing, prefixing a URL with '& ' will put it in Duke's history and return you to it, assuming you've setup Duke as your default search.

Side note: In reading that link to double-bucky, I realized I've used that term incorrectly (including right here, just now!) my whole life. I'm an imposter of an imposter.

`&-` is a toy that tries to turn a URL for which you have an alias into more readable input. For example, let's say you have an alias like this: `alias acmeship https://acmeshippingco.com/trackpackage.cgi?trackingNo=${*}` and you get an email from your favorite online retailer with a link to a string of click tracking sites that eventually dumps you at `https://acmeshippingco.com/trackpackage.cgi?trackingNo=12340987ZZT0P`. At that URL, you can ^l to focus the URL bar, Home (or w/e) to get to the front of the line and prefix the whole thing with `&-` and Duke *should* be able to put into your history `acmeship 12340987ZZT0P` but URLs are inconsistent and nothing is perfect. If Duke fails to deconstruct the URL, the entire URL will be put in history, so you're good either way.

If you have a simple alias like `alias whatever http://whatevz.lol` and you're at that destination and you use, for some weird reason, `&-` it'll deconstruct that too. I got you, fam.

## Preferences

* `set <PREFNAME> <PREFVALUE>`
* `unset <PREFNAME>`
* `unset <PREFPREFIX>*`
* `saveui <ALIAS NAME>`

* `bookmarks:show` (int) The number of recent bookmarks to show in the UI
* `default:destination` (Alias) If you type something that Duke doesn't understand, use this alias to resolve it. Search engine suggested.
* `history:show` (int) History is infinite, but only show the last N
* `history:verbose` (true|false) show a longer output for history.
* `ui:background` (any CSS-valid color designator) the background color of the UI
* `ui:button:color` (any CSS-valid color designator) The CSS-valid color for buttons and probably some other stuff.
* `ui:button:danger:color` (any CSS-valid color designator) The CSS-valid color for SCARY buttons and probably some other stuff.
* `ui:button:text:color` (any CSS-valid color designator)The CSS-valid color for button text.
* `ui:font:primary` (any CSS-valid font list) CSS-valid list of font-family fonts used by most of the UI
* `ui:font:size` (int) This number will have "px" appended to it to set the overall font size for the app.
* `ui:highlight` (any CSS-valid color designator) The CSS-valid color used for accents here and there.
* `ui:hovershadow` (any CSS-valid color designator) The CSS-valid color used for hover indicators. Set to `0` to disable these hover indicators.
* `ui:link:color` (any CSS-valid color designator) The CSS-valid color used for link text.
* `ui:text:color` (any CSS-valid color designator) The CSS-valid text color.

There's more. Check the preferences page by clicking the gears or typing `preferences`.

Setting a preference to the current default deletes the preference. If the default changes, you might be surprised.

You can do fun stuff with `ui:background` and `ui:button:color`(&:danger) like `set ui:background linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%)`

You can unset swaths of prefs with wildcards like `unset ui:*` or `unset ui:font*`.

You can save whatever UI prefs you've set into a multi-command `set` alias with `saveui <ALIAS NAME>` which will give you a chance to edit the alias before committing.

For UI customization preferences, you can use variables from [OpenProps](https://open-props.style/).

## Home Assistant Integration

Connect Duke to your HASS and turn stuff on and off like a caveman with a keyboard. Set the following preferences:

1. `hass:token` Create a token at the bottom of your HASS profile.
1. `hass:url` The URL for your HASS instance including port (unless it's 80 or 443) but with NO trailing slash.
1. `hass:ignoreTLS` (true|false) Ignore self-signed/invalid certs.

Commands:

1. `ha on <ENTITY>` eg: `ha on script.arm_warheads`
1. `ha off <ENTITY>` eg: `ha off light.office_lamp`
1. `ha toggle <ENTITY>` eg: `ha toggle switch.kauf_plug_22`


# Half implemented

## Plugins
In app/ in a dir called 'plugins' create an .rb file. It doesn't matter what it's called.

(This was all written before the HASS plugin... use it as an example.)

It should contain a Ruby module called whatever your plugin is called. For example:

```ruby
module Bobo
end
```

This module will be `module_eval`ed into the `DukeOfUrl::Plugins` namespace. This is done to make plugin writing low-friction, but if it ends up having problems I may end up making plugin authors manually namespace their stuff into that namespace.

This has the side-effect of requiring input handlers to be fully qualified:

```ruby
module Bobo
  module ::DukeOfUrl::Input
    module Handlers
      class Faker < AbstractHandler

        def self.verb
          %q{bobofaker}
        end

        def message
          %q{That's one song in the bank.}
        end

        def redirect?
          false
        end

      end
    end
  end
end
```

Also because Ruby is weird, if you were to try to use `module ::DukeOfUrl::Input::Handlers` (written all at once like that) it won't be able to find `AbstractHandler` without fully qualifying it. The perils of abusing constants, I guess.

Similarly, if you want to register Preferences, you'll need to fully qualify:

```ruby
module Bobo
  ::DukeOfUrl::Preferences.register( {
    # ...
  })
end
```

If your plugin wants routes, just create a Routes module:

```ruby
module Bobo
  module Routes
    def self.registered(app)

      app.get("/bobo/secret") do
        %Q{It's a secret to everybody!}
      end

    end
  end
end
```

I haven't really solved plugins having views yet, so yea... half-implemented.


# to be implemented

## Backups

`backup` creates `userdata/DATE/` and copies your data in there

maybe a downloadable tarball or json export?


