ARG BUNDLE_ONLY=production
ARG UID=4567

FROM ruby:3.2.3-slim-bookworm as build

ARG BUNDLE_ONLY
ARG UID
ENV BUNDLE_ONLY=${BUNDLE_ONLY}
ENV GEM_HOME /app/vendor/bundle
ENV BUNDLE_PATH $GEM_HOME
ENV PATH $GEM_HOME/bin:$PATH
WORKDIR /app

RUN apt update && apt install -y  build-essential ruby-dev libffi-dev libpq-dev\
  && adduser --uid ${UID} --home /app duke && chown duke:users /app
COPY --chown=duke:users app /app
USER duke
RUN bundle config set without 'development test' && bundle _2.4.19_ install


FROM ruby:3.2.3-slim-bookworm

ARG BUNDLE_ONLY
ARG UID
ENV BUNDLE_ONLY=${BUNDLE_ONLY}
ENV GEM_HOME /app/vendor/bundle
ENV PATH $GEM_HOME/bin:$PATH
ENV BUNDLE_PATH $GEM_HOME
WORKDIR /app

RUN adduser --uid ${UID} --home /app duke
COPY --from=build /app /app
EXPOSE 4567

USER duke
CMD ["bundle", "exec", "puma", "-e", "production", "-b", "tcp://0:4567"]
