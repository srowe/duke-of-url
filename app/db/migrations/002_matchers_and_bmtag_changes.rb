Sequel.migration do
  up do

    updates = from(:bmtags).map do |hash|
      hash[:tag] = hash[:tag].sub(/\s+/,'-')
      hash
    end
    updates.each do |update|
      from(:bmtags).where( tid: update[:tid]).update(update)
    end

    updates = from(:history).select(:id, :text).map{|h| h[:text] = h[:text].squeeze(' '); h}
    updates.each do |update|
      from(:history).where( id: update[:id]).update(text: update[:text])
    end

    create_table(:matchers) do
      primary_key :id
      String :label, null: false
      String :matcher, null: false
      String :flags
      String :template, null: false
      String :type, null: false
      DateTime :recent, null: false
      DateTime :orig, null: false
    end

    aliases = from(:aliases).map do |old|
      matcher = %Q{^#{old[:verb]}$}
      matcher = %Q{^#{old[:verb]} +(.*)}.squeeze(' ') if old[:template].include?("${*}")

      row = {
        label: old[:verb].strip,
        matcher: matcher,
        flags: '',
        template: old[:template],
        type: 'alias',
        orig: old[:orig],
        recent: old[:recent],
      }
      from(:matchers).insert(row)
    end

    drop_table(:aliases)

  end
end
