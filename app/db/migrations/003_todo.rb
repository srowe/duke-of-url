Sequel.migration do
  up do
    create_table(:todo) do
      primary_key :id
      String :task, null: false
      DateTime :created, null: false
    end
  end
end
