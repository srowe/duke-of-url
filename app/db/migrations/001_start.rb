Sequel.migration do
  change do
    create_table?(:aliases) do
      primary_key :id
      String :verb, :size=>255, :null=>false
      String :template, :size=>255, :null=>false
      DateTime :recent, :null=>false
      DateTime :orig, :null=>false
    end

    create_table?(:bm, :ignore_index_errors=>true) do
      primary_key :id
      String :url, :size=>255, :null=>false
      String :notes, :size=>255

      index [:url]
    end

    create_table?(:bmtags) do
      primary_key :tid
      String :tag, :size=>255, :null=>false
      Integer :bmid, :null=>false
    end

    create_table?(:history) do
      primary_key :id
      String :text, :size=>255, :null=>false
      DateTime :recent, :null=>false
      DateTime :orig, :null=>false
      Integer :visit_count
    end

    create_table?(:prefs) do
      primary_key :id
      String :key, :size=>255, :null=>false
      String :value, :size=>255, :null=>false
    end
  end
end
