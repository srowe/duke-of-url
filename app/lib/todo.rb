module DukeOfUrl
  module ToDo

    ::DukeOfUrl::Preferences.register( {
      'todo:sort' => { default: 'asc', description: 'Sort the todo items', valid: /(asc|desc)/, suggestions: %w{asc desc} },
    })

    def self.read
      pref = ::DukeOfUrl::Preferences.fetch('todo:sort').to_sym
      order = Sequel.send(pref,:created)
      Item.order(order).all
    end

    class Item < Sequel::Model(DUKE_DB[:todo])
      def self.remove!(raw_input)
        self.where(task: raw_input).first.destroy
      end

      def self.save!(raw_input)
        self.create(task: raw_input.strip, created: DateTime.now)
      end
    end

    module Routes
      def self.registered(app)

        app.get("/todo/") do
          @todo = DukeOfUrl::ToDo.read
          slim :todo, layout: :layout
        end

        app.post('/deletetodo/:todoid') do
          id = params['todoid'].to_i
          bye = ToDo::Item.where(id: id).first
          bye.destroy
          ''
        end

      end
    end

  end
end
