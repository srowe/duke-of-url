module DukeOfUrl
  module Matchers

    ::DukeOfUrl::Preferences.register( {
      'matchers:casesensitive' => { default: 'true', description: 'Case sensitivity setting when <strong>creating</strong> a new matcher.', valid: ::DukeOfUrl::Preferences::Valid_Bool, suggestions: %w{false true} },
    })

    def self.find(raw_input)
      notclashing = read.find{|mat| mat.rematch.match?(raw_input.strip)}
      return raw_input unless notclashing
      notclashing.raw_input = raw_input
      notclashing
    end

    def self.aliases
      Alias.order_by(:matcher).where(type: 'alias')
    end

    def self.matched?(raw_input)
      type = ::DukeOfUrl::Preferences.fetch('history:matcher')
      type = "alias" if type == "aliases"
      type = %w{advanced alias} if type == 'all'
      matchers = read.where(type: type).all
      unless type == 'alias'
        notclashing = matchers.find{|mat| mat.rematch.match?(raw_input)}
        return notclashing if notclashing
      end
      notclashing = matchers.find{|mat| mat.template == raw_input}
      return notclashing if notclashing
      false
    end

    def self.read
      Matcher.order_by(:matcher)
    end

    def self.save!(input)
      Matcher.save!(input)
    end

    def self.save_alias!(input)
      Alias.save!( input )
    end

    def self.typeahead
      aliases.all
    end

    module Routes
      def self.registered(app)
        app.post "/matcher_delete/:mid" do
          id = params['mid'].to_i
          bye = Matcher[id]
          bye.destroy
          ''
        end

        app.get "/aliases/" do
          slim :aliases, layout: :layout
        end

        app.get "/matchers/" do
          @matchers = ::DukeOfUrl::Matchers.read.sort_by{|matcher| [matcher.type, matcher.label]}
          slim :matchers, layout: :layout
        end

        app.get "/matcher_edit/:matcherlabel" do
          now   = DateTime.now
          flags = ::DukeOfUrl::Preferences.fetch('matchers:casesensitive') == 'true' ? '' : 'i'
          @matcher = Matcher.where(label: params[:matcherlabel]).first || Matcher.new(label: params[:matcherlabel], matcher: '', flags: flags, template: '', type: 'advanced', recent: now, orig: now)
          slim :matcher_editor, layout: :layout
        end

        app.post "/matcher_save" do
          # this is stolen from Matcher#save... DRY it up
          now  = DateTime.now
          flags = params.key?(:casesensitive) ? '' : 'i'
          args = {
            label:    params[:label],
            matcher:  params[:matcher],
            flags:    flags,
            template: params[:template],
            type:     params[:type],
            recent:   now
          }
          if og = Matcher.find(label: params[:label])
            og.update( args )
          else
            Matcher.create( args.merge(orig: now) )
          end
          redirect '/'
        end
      end
    end

    class Matcher < Sequel::Model(DUKE_DB[:matchers])
      POSITIONAL_DESTINATION_RE = /\$\{(\d+)\}/
      def self.save!(raw_input)
        now  = DateTime.now
        type = 'advanced'

        label, matcher, template = *::DukeOfUrl::Input::Parser.new(raw_input).shell_words
        return %q{match: invalid input} if label.nil?

        args = {label: label, matcher: matcher, flags: '', template: template, type: type, recent: now}
        if og = self.find(label: label)
          og.update( args )
        else
          self.create( args.merge(orig: now) )
        end
        %Q{#{label} set to send #{matcher} matches to #{template}}
      end

      def args?
        template.include?('${*}') || template.match?(/\$\{\d+\}/)
      end

      def debug
        %Q{#{self.class.to_s.split('::').last}: #{label}, $*: "#{dollar_star}", pos_input: #{positional_input_args}, destination: #{destination}}
      end

      def destination
        all = template.sub('${*}',dollar_star)
        positional_destination_args.each do |nplusone|
          nplusone    = nplusone.to_i
          re          = Regexp.new("\\\$\\\{#{nplusone}\\\}")
          replacement = positional_input_args.fetch(nplusone -1, 'NoMatchError')
          all         = all.sub(re, replacement)
        end
        all
      end

      def dollar_star
        matched = rematch.match(raw_input)
        return escape matched.captures.last if matched
        # this is for default-destination
        escaped_args
      end

      def escape(thing)
        ERB::Util.url_encode(thing)
      end

      def history?
        true
      end

      def persist!
        DukeOfUrl::History.save!(raw_input) if history?
      end

      def positional_destination_args
        @positional_destination_args ||= template.scan(POSITIONAL_DESTINATION_RE).flatten
      end

      def positional_input_args
        matched = rematch.match(raw_input)
        return matched.captures if matched
        []
      end

      def raw_input=(raw_input)
        @raw_input = raw_input
      end

      def redirect?
        template_is_url?
      end

      def rematch
        ruby_bug = flags == "" ? nil : flags
        # if you pass an empty string to Regexp.new in 3.1.2 it turns on case insensitivity
        Regexp.new(matcher, ruby_bug)
      end

      private

      def escaped_args
        ERB::Util.url_encode(raw_input)
      end

      def raw_input
        @raw_input
      end

      def template_is_url?
        template.match?(VALID_URL_RE)
      end

    end

    class Alias < Matcher
      ARGS = '${*}'
      def self.save!(raw_input)
        now = DateTime.now
        type = 'alias'

        label, *template = *::DukeOfUrl::Input::Parser.new(raw_input).words
        return %q{alias: invalid input} if label.nil?
        template = template.join(" ")
        matcher  = %Q{^#{label}$}
        if template.include?("${*}")
          matcher = %Q{^#{label} +(.*)}
        end

        args = {label: label, matcher: matcher, template: template, type: type, orig: now, recent: now}
        if og = self.find(label: label)
          og.update( args )
        else
          self.create( args )
        end
        %Q{#{label} set to #{template}}
      end

      def arg_location
        template.index(ARGS) || 0
      end

      def before_destroy
        pref = %q{default:destination}
        Preferences.unset(pref) if Preferences.fetch(pref) == verb.strip
      end

      def first_letter
        fl = label[0]
        return '00' if fl.to_i.to_s == fl
        fl
      end

      def might_decode?(input)
        u_template = bobojojo
        u_template = bobojojo.sub(/([^?]+).*/,'\1') if template.include?('?')
        u_template = u_template.sub(/BOBOJOJO/,'.*')

        Regexp.new(u_template).match?(input)
      end

      def origin
        template_uri.origin
      end

      # for default:destination pref
      def raw_input=(raw_input)
        @raw_input = raw_input
      end

      # for edit alias command
      def to_s
        %Q{#{verb} #{template}}
      end

      def typeahead_match?(raw_input)
        verb.start_with?(raw_input)
      end

      def typeahead_suggestion
        TypeAheadSuggestion.new(verb,usage)
      end

      def uh_oh_persist!
        # if used by UhOh, the default alias will be prepended
        args = ::DukeOfUrl::Input::Parser.new(raw_input).args
        DukeOfUrl::History.save!(args) if history?
      end

      def unwind(input)
        # if you're a simple alias without arguments
        return verb unless args?
        user_uri     = URI.parse(input)
        user_capture = ''
        # you have arguments that are a query string
        if template.include?('?')
          expander_arg = CGI.parse(template_uri.query).key(['BOBOJOJO'])
          if expander_arg
            user_capture = CGI.parse(user_uri.query).fetch(expander_arg,nil)&.first
          end
        # you have arguements that are part of the request path
        else
          post_args    = template[arg_location+4..-1]
          user_capture = input[arg_location..-1].sub(post_args,'')
        end
        return %Q{#{verb} #{user_capture}} unless user_capture.empty?
        input
      end

      def usage
        return %Q{Usage: <strong>#{verb}</strong> <em>&lt;ARGS&gt;</em>} if args?
        %Q{Usage: <strong>#{verb}</strong>}
      end

      def verb
        label
      end

      private

      def args
        raw_input.sub(label,'').lstrip
      end

      def bobojojo
        @bobojojo ||= template.sub(ARGS,'BOBOJOJO')
      end

      def escaped_args
        ERB::Util.url_encode(args)
      end

      def template_uri
        @template_uri ||= URI.parse(bobojojo)
      end

    end
  end

end
