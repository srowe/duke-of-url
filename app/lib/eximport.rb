module DukeOfUrl
  module Export

    def self.export(style=:export)
      {
        aliases: Aliases.read.map(&style),
        bookmarks: ::DukeOfUrl::Bookmarks::Bookmark.all.map(&style),
        history: History.typeahead.map(&style),
      }
    end

    module Routes
      def self.registered(app)
        app.get "/export.:format" do
          return '' unless params[:format]


          case params[:format]
          when 'input'
            content_type 'text/plain'
            Export.export(:export_as_input).values.flatten.join("\n")
          when 'json'
            content_type 'application/json'
            Export.export.to_json
          when 'txt', 'yaml'
            content_type 'text/plain'
            Export.export.to_yaml
          else
            ''
          end
        end

      end
    end

  end

  module Import
  end
end
