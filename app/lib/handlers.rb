module DukeOfUrl
  module Input

    class AbstractHandler
      include ::DukeOfUrl::Parsers::UserInput

      def self.for(input)
        input.start_with?(verb)
      end

      def self.verb
        ''
      end

      def initialize(raw_input)
        @raw_input = raw_input
      end

      def debug
        out  = %Q{#{self.class.to_s.split('::').last}}
        out += %Q{, desination: #{destination}, redirect?: #{redirect?}} if redirect?
        out += %Q{, message: #{message}} if (! message.nil?) && (message != '')
        out
      end

      def destination
      end

      def message
      end

      def persist!
        DukeOfUrl::History.save!(input) if history?
      end

      def redirect?
        true
      end

      def usage
        ''
      end

      def typeahead_match?(_) #it already knows the input
        return false if self.class.to_s == 'AbstractHandler'
        return verb.start_with?(raw_input) if raw_input.length < verb.length
        raw_input.start_with?(verb)
      end

      def typeahead_suggestion
        [::DukeOfUrl::TypeAheadSuggestion.new(verb, usage)]
      end


      def verb
        return self.class.verb if self.class.respond_to?(:verb)
        ''
      end

      private

      def history?
        false
      end

    end

    class MultiCmd < AbstractHandler
      def self.for(input)
        input.include?(" && ") && ! input.start_with?("alias ")
      end

      def commands
        @commands ||= raw_input.split(' && ').map{|cmd| ::DukeOfUrl::Input.process(cmd)}
      end

      def destination
        finally.destination
      end

      def message
        finally.message
      end

      def persist!
        commands.each(&:persist!)
      end

      def redirect?
        finally.redirect?
      end

      private

      def finally
        @finally ||= commands.last
      end
    end


    module Handlers

      class URL < AbstractHandler

        def self.for(input)
          input.match?(VALID_URL_RE)
        end

        def destination
          raw_input
        end

        private

        def history?
          ! leading_space?
        end
      end

      class Math < AbstractHandler
        IS_MATH = /^[\s\d\(\)\+%\/*.-]+$/

        def self.for(input)
          input.match?(IS_MATH)
        end

        def self.verb
          # This method is unused and this string exists to trick the typeahead code.
          %q{&nbsp;}
        end

        def message
          begin
            eval(input)
          rescue SyntaxError => e
            "Usage: &lt;VALID MATH EXPRESSION&gt;"
          end
        end

        def redirect?
          false
        end

        def typeahead_match?(_)
          self.class.for(raw_input)
        end

        alias_method :usage, :message
      end

      class TLD < AbstractHandler
        # https://jecas.cz/tld-list/?type=plain
        LOL = JSON.parse(File.read('lib/tlds.json'))
        def self.for(input)
          input = input.upcase
          ! input.strip.match(/\s/) && LOL.find{|tld| input.end_with?(".#{tld}")}
        end

        def destination
          %Q{https://#{input}}
        end

        private

        def history?
          ! leading_space?
        end
      end

      class Alias < AbstractHandler

        def self.verb
          %q{alias }
        end

        def message
          @message
        end

        def persist!
          @message = Matchers.save_alias!(args)
        end

        def redirect?
          false
        end

        def usage
          %Q{Usage: <strong>#{verb}</strong> <em>&lt;URL or command&gt;</em>}
        end

      end

      class AliasReturn < Alias
        def self.verb
          %q{aliasr }
        end

        def destination
          last_word
        end

        def redirect?
          true
        end

        def usage
          super + %Q{ This command will return you to the site you aliased.}
        end
      end

      class AliasBrowser < AbstractHandler

        def self.verb
          %q{aliases}
        end

        def destination
          '/aliases/'
        end
      end

      class AliasEditor < AbstractHandler

        def self.verb
          %q{edit alias }
        end

        def form_value
          %Q{alias #{Matchers::Alias.find(label: args)}}
        end

        def redirect?
          false
        end

        def typeahead_suggestion
          return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage) if raw_input.length < verb.length

          targets = ::DukeOfUrl::Matchers::Alias.all
          targets = ::DukeOfUrl::Matchers::Alias.where( Sequel.like(:label, %Q{#{last_word}%}) ) if last_word != verb.strip

          targets.map do |target|
            ::DukeOfUrl::TypeAheadSuggestion.new(%Q{#{verb}#{target.verb}}, usage)
          end
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;ALIAS&gt;</em>}
        end

      end

      class UnAlias < AbstractHandler

        def self.verb
          %q{unalias }
        end

        def persist!
          maybestr = Matchers::Alias.find(label: words[1])
          maybestr.destroy if maybestr.respond_to?(:destroy) end

        def redirect?
          false
        end

        def typeahead_suggestion
          return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage) if raw_input.length < verb.length

          targets = ::DukeOfUrl::Matchers::Alias.all
          targets = ::DukeOfUrl::Matchers::Alias.where( Sequel.like(:label, %Q{#{last_word}%}) ) if last_word != verb.strip

          targets.map do |target|
            ::DukeOfUrl::TypeAheadSuggestion.new(%Q{#{verb}#{target.verb}}, usage)
          end
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;ALIAS&gt;</em>}
        end

      end

      class Debug < AbstractHandler
        def self.verb
          %q{debug }
        end

        def message
          ::DukeOfUrl::Input.process(args).debug
        end

        def redirect?
          false
        end

        def typeahead_match?(_)
          false
        end
      end

      class History < AbstractHandler

        def self.for(input)
          input.strip == verb
        end

        def self.verb
          %q{history}
        end

        def destination
          %q{/history/}
        end
      end

      class HistorySearch < AbstractHandler

        def self.verb
          %q{history? }
        end

        def destination
          %Q{/history/?q=#{escaped_args}}
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;SEARCHTEXT&gt;</em>}
        end

      end

      class JustGo < AbstractHandler
        def self.verb
          %q{& }
        end

        def destination
          last_word
        end

        def persist!
          DukeOfUrl::History.save!(destination)
        end

        def redirect?
          true
        end

        def typeahead_match?(_)
          false
        end
      end

      class AliasFromUrl < AbstractHandler
        def self.verb
          %q{&- }
        end

        def decode
          found = Matchers.aliases.find{|alius| alius.might_decode?(args)}
          return args unless found
          found.unwind(args)
        end

        def persist!
          DukeOfUrl::History.save!(decode)
        end

        def redirect?
          false
        end

        def typeahead_match?(_)
          false
        end
      end

      class Matcher < AbstractHandler

        def self.verb
          %q{match }
        end

        def message
          @message
        end

        def persist!
          @message = Matchers.save!(args)
        end

        def redirect?
          false
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;LABEL&gt;</em> <em>&lt;MATCH_REGEX&gt;</em> <em>&lt;TEMPLATE&gt;</em> (using <strong>edit matcher</strong> strongly preferred over this command.)}
        end

      end

      class MatcherEditor < AbstractHandler

        def self.verb
          %q{edit matcher }
        end

        def destination
          %Q{/matcher_edit/#{args}}
        end

        def redirect?
          true
        end

        def typeahead_suggestion
          return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage) if raw_input.length < verb.length

          targets = ::DukeOfUrl::Matchers::Matcher.where( type: 'advanced' )
          targets = ::DukeOfUrl::Matchers::Matcher.where( type: 'advanced').where(Sequel.like(:label, %Q{#{last_word}%}) ) if last_word != 'matcher'

          targets.map do |target|
            ::DukeOfUrl::TypeAheadSuggestion.new(%Q{#{verb}#{target.label}}, usage)
          end
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;MATCHER LABEL&gt;</em>}
        end

      end

      class MatcherBrowser < AbstractHandler
        def self.verb
          %q{matchers}
        end

        def destination
          '/matchers/'
        end
      end

      class TodoBrowser < AbstractHandler
        def self.verb
          %q{todo}
        end

        def destination
          '/todo/'
        end
      end

      class TodoDo < AbstractHandler
        def self.verb
          %q{do }
        end

        def persist!
          ::DukeOfUrl::ToDo::Item.save!(args)
        end

        def usage
          %Q{Usage: #{verb} <em>Task description</em>}
        end
      end

      class TodoDone < AbstractHandler
        def self.verb
          %q{done }
        end

        def persist!
          ::DukeOfUrl::ToDo::Item.remove!(args)
        end

        def typeahead_suggestion
          ::DukeOfUrl::ToDo::Item.where(Sequel.like(:task, %Q{%#{args}%})).map do |item|
            ::DukeOfUrl::TypeAheadSuggestion.new(%Q{#{verb}#{item.task}}, usage)
          end
        end

        def usage
          %Q{Usage: #{verb} <em>Task description</em>}
        end
      end

      class UnMatch < AbstractHandler

        def self.verb
          %q{unmatch }
        end

        def persist!
          maybestr = Matchers::Matcher.find(label: words[1])
          maybestr.destroy if maybestr.respond_to?(:destroy) end

        def redirect?
          false
        end

        def typeahead_suggestion
          return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage) if raw_input.length < verb.length

          targets = ::DukeOfUrl::Matchers::Matcher.where( type: 'advanced').all
          targets = ::DukeOfUrl::Matchers::Matcher.where( type: 'advanced').where( Sequel.like(:label, %Q{#{last_word}%}) ) if last_word != verb.strip

          targets.map do |target|
            ::DukeOfUrl::TypeAheadSuggestion.new(%Q{#{verb}#{target.label}}, usage)
          end
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;MATCHER&gt;</em>}
        end

      end


    end

  end
end
