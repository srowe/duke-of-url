module DukeOfUrl
  module Bookmarks

    ::DukeOfUrl::Preferences.register( {
      'bookmarks:show' => {default: 20, description: 'The number of bookmarks to show in the UI.', valid: /^\d+/ },
      'bookmarks:add2history' => {default: true, description: 'Add or update a history entry for the bookmarked url when saving or updating a bookmark.', valid: ::DukeOfUrl::Preferences::Valid_Bool, suggestions: %w{true false}},
    } )

    def self.retag!(shell_words_args)
      oldtag, *newtag = shell_words_args
      newtag = Array(newtag).join(' ') #if the user failed to quote
      Tag.where(tag: oldtag).update(tag: newtag)
    end

    def self.save!(raw_input)
      Bookmark.save!(raw_input)
    end

    def self.bookmarked?(url)
      Bookmark.where(url: url)
    end

    class Bookmark < Sequel::Model(DUKE_DB[:bm])
      def self.save!(raw_input)
        url, *tags = *raw_input.split

        tags = tags.map(&:strip)

        ::DukeOfUrl::History.save!(url) if ::DukeOfUrl::Preferences.bool('bookmarks:add2history')

        bm = self.where(url: url).first
        if bm
          Tag.where(bmid: bm.id).all.each{|tag| tag.destroy}
        else
          bm = self.create(url: url, notes: '')
        end
        bmid = bm.id
        tags.each do |tag|
          Tag.create(tag: tag, bmid: bmid)
        end
      end

      def self.with_tags(tags)
        tagcount = Array(tags).length
        self.from(:bm, :bmtags).group_and_count(:bm__id).where(bmtags__bmid: :bm__id).where(bmtags__tag: tags).having{count.function.* >= tagcount}.select_append(:bm__url).all
      end

      def encoded_url
        ERB::Util.url_encode(url)
      end

      def export
        {
          tags: tags.map(&:tag),
          url: url,
        }
      end

      def export_as_input
        %Q{bm #{url} #{tags.map(&:tag).join(" ")}}
      end

      def tags
        Tag.where(bmid: id).sort_by(&:tag)
      end

      def typeahead_match?(_)
        true
      end

      def typeahead_suggestion
        ::DukeOfUrl::TypeAheadSuggestion.new(url, %Q{From a bookmark tagged: #{tags.map(&:tag).join(' ')}})
      end

    end

    class Tag < Sequel::Model(DUKE_DB[:bmtags])
      def self.cloud
        # "SELECT `tag`, count(*) AS 'c' FROM `bmtags` GROUP BY `tag`"
        self.group_and_count(:tag)
      end

      def bookmark
        Bookmark.where(id: bmid).first
      end

      def bookmark_count
        Tag.where(tag: tag).count
      end

      def cloud_factor(max_value)
        max_font_size    = 80.0
        min_font_size    = 8.0
        range            = max_font_size - min_font_size
        countf           = count.to_f
        factor           = (countf / max_value)
        scaled_font_size = (range * factor) + min_font_size - (min_font_size * factor)
        %Q{#{scaled_font_size}pt}

      end

      def count
        @values.fetch(:count,0)
      end

      def encoded_tag
        ERB::Util.url_encode(tag)
      end

      def to_json(*a)
        %Q{"#{tag}"}
      end
    end


    module Routes
      def self.registered(app)

        app.get '/bookmarks/' do
          @params = params

          if params['bms']
            parsed = ::DukeOfUrl::Input::Parser.new(params['bms'])
            wild = %Q{%#{parsed.args}%}
            @bookmarks = Bookmark.order_by(Sequel.desc(:id)).where(Sequel.like(:url, wild)).all
          else
            @bookmarks = Bookmark.order_by(Sequel.desc(:id)).all
          end

          slim :bookmarks, layout: :layout
        end

        app.post '/delbookmark/:bmid' do
          id = params['bmid'].to_i
          the = Bookmark.where(id: id).first
          the.destroy
          Tag.where(bmid: id).each{|tag| tag.destroy}
          ''
        end

        app.get '/bookmarks/:tags' do
          tags = params["tags"].split(',').map(&:strip)
          @bookmarks = Bookmark.with_tags(tags)
          slim :bookmarks, layout: :layout
        end

      end
    end
  end

  module Input
    module Handlers

      class Bookmark < AbstractHandler

        def self.verb
          %q{bm }
        end

        def persist!
          Bookmarks.save!(args)
        end

        def redirect?
          false
        end

        # this is identical to the BookmarkRetag implementation
        def typeahead_suggestion
          # 3 because ['bm','https....']
          if raw_input.length < verb.length || last_word == verb.strip || words.length < 3
            return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage)
          end

          last_tags   = ::DukeOfUrl::Bookmarks::Tag.all
          last_tags   = ::DukeOfUrl::Bookmarks::Tag.where( ::Sequel.like(:tag, %Q{#{last_word}%})) if last_word != verb.strip
          last_tags = last_tags.map(&:tag).uniq

          last_tags.map do |last_tag|
            ::DukeOfUrl::TypeAheadSuggestion.new(raw_input.sub(/#{last_word}$/,last_tag), usage)
          end
        end

        def usage
          %Q{Usage: <strong>#{verb}</strong> <em>&lt;URL&gt;</em> <em>&lt;TAG&gt;[ &lt;TAG&gt;...]</em>}
        end

      end

      class BookmarkReturn < Bookmark

        def self.signature
          %q{^bmr\s+\S+}
        end

        def self.verb
          %q{bmr }
        end

        def destination
          words[1]
        end

        def redirect?
          true
        end

        def usage
          super + %Q{ This command will return you to the site you bookmarked.}
        end
      end

      class BookmarkBrowser < AbstractHandler

        def self.for(input)
          input.strip == verb
        end

        def self.verb
          %q{bms}
        end

        def destination
          %Q{/bookmarks/}
        end

      end

      class BookmarkRetag < AbstractHandler

        def self.verb
          %q{bmretag }
        end

        def message
          %Q{"#{shell_words_args.first}" retagged to #{shell_words_args[1..-1].join(' ')}}
        end

        def persist!
          Bookmarks.retag!(shell_words_args)
        end

        def redirect?
          false
        end

        def typeahead_suggestion
          if raw_input.length < verb.length || last_word == verb.strip
            return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage)
          end

          last_tags = ::DukeOfUrl::Bookmarks::Tag.all
          last_tags = ::DukeOfUrl::Bookmarks::Tag.where( ::Sequel.like(:tag, %Q{#{last_word}%})) if last_word != verb.strip

          last_tags = last_tags.map(&:tag).uniq
          last_tags.map do |last_tag|
            ::DukeOfUrl::TypeAheadSuggestion.new(raw_input.sub(/#{last_word}$/,last_tag), usage)
          end
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;OLD TAG&gt;</em> <em>&lt;NEW TAG&gt;}
        end

      end

      class BookmarkSearch < AbstractHandler

        def self.verb
          %q{bm? }
        end

        def destination
          %Q{/bookmarks/?bms=#{escaped_args}}
        end

        def usage
          %Q{Usage: #{verb} <em>&lt;SEARCH TERM&gt;</em>}
        end

      end

      class BookmarkTags < AbstractHandler

        def self.verb
          %q{bmt }
        end

        def initialize(raw_input)
          @raw_input = raw_input
        end

        def destination
          %Q{/bookmarks/#{escaped_args}}
        end

        def typeahead_suggestion
          if raw_input.length < verb.length || last_word == verb.strip
            return ::DukeOfUrl::TypeAheadSuggestion.new(verb,usage)
          end

          last_tags = ::DukeOfUrl::Bookmarks::Tag.all
          last_tags = ::DukeOfUrl::Bookmarks::Tag.where( ::Sequel.like(:tag, %Q{#{last_word}%})) if last_word != verb.strip

          last_tags = last_tags.map(&:tag).uniq
          last_tags.map do |last_tag|
            ::DukeOfUrl::TypeAheadSuggestion.new(raw_input.sub(/#{last_word}$/,last_tag), usage)
          end
        end

        def usage
          %Q{Find bookmarks by one or more tags. Usage: #{verb} <em>&lt;BOOKMARK TAG&gt;</em> [...<em>&lt;BOOKMARK TAG&gt;</em>]}
        end

      end

    end
  end
end
