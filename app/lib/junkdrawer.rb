module DukeOfUrl
  class NullObj
    def initialize(response)
      @response = response
    end
    def method_missing(*args)
      @response
    end
  end

  class TypeAheadSuggestion
    def initialize(suggestion, usage)
      @suggestion = suggestion
      @usage      = usage
    end

    def <=>(other)
      [slength,suggestion] <=> [other.slength, other.suggestion]
    end

    def eql?(other)
      other.suggestion == suggestion
    end

    def hash
      suggestion.hash
    end

    def slength
      suggestion.length
    end

    def suggestion
      @suggestion.squeeze(' ')
    end

    def to_json(*a)
      {
        suggestion: suggestion,
        usage: usage,
      }.to_json(*a)
    end

    def usage
      @usage
    end
  end

  module Formatters
    module Seconds
      UNITS = [
        [60, :seconds],
        [60, :minutes],
        [24, :hours],
        [1000, :days],
      ]
      def self.format(secs)
        suffix = ago(secs)
        secs = secs.abs
        UNITS.map do |count, name|
          next unless secs > 0
          secs, quo = secs.divmod(count)
          "#{quo.to_i} #{name}"
        end.compact.last(2).reverse.join(' ') + suffix # the call to #last makes it less specific, but still suitable for my needs
      end
      def self.ago(secs)
        return ' ago' if secs < 0
        ''
      end
    end
  end

  module Parsers
    module UserInput

      def args
        raw_input.sub(first_word_re,'').strip
      end

      def escape(thing)
        ERB::Util.url_encode(thing)
      end

      def escaped_args
        escape(args)
      end

      def escaped_input
        escape(raw_input)
      end

      def first_word
        words.first
      end

      def first_word_re
        return /^\s*#{Regexp.escape(verb)}\s*/ unless verb.empty?
        /^\s*\S+\s+/
      end

      def input
        raw_input.strip
      end

      def last_word
        words.last
      end

      def leading_space?
        raw_input.match?(/^\s+/)
      end

      def raw_input
        @raw_input
      end

      def recursive?
        shell_words[1..-1].include?(first_word) || first_word == 'alias'
      end

      def shell_words
        @shell_words ||= Shellwords.split(raw_input)
      end

      def shell_words_args
        @shell_words_args ||= Shellwords.split(args)
      end

      def words
        @words ||= raw_input.split
      end

    end
  end
end

