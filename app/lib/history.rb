module DukeOfUrl

  module History

    ::DukeOfUrl::Preferences.register( {
      'history:show' => {default: 20, description: 'The number of history entries to show in the UI.', valid: /^\d+/ },
      'history:bookmarktags' => { default: 'true', description: 'Show bookmark tags of bookmarked history entries. False == faster.', valid: ::DukeOfUrl::Preferences::Valid_Bool, suggestions: %w{false true} },
      'history:matcher' => { default: 'off', description: 'Show matched matcher of history entries. When set to "aliases," only show matcher labels when the template of the matcher matches exactly. When set to "all" show both matched templates and matched matchers. Off == faster.', valid: /(?:off|all|aliases)/, suggestions: %w{off all aliases} },
      'history:verbose' => { default: 'false', description: 'Show visit statistics for history entries.', valid: ::DukeOfUrl::Preferences::Valid_Bool, suggestions: %w{false true} },
    })

    def self.read(limit=10)
      HistoryEntry.most_recent(limit)
    end

    def self.save!(input)
      HistoryEntry.save!(input)
    end

    def self.typeahead
      HistoryEntry.default_sort.all
    end

    module Routes
      def self.registered(app)
        app.get "/history/" do
          if params["q"] && ! params["q"].strip.empty?
            @history = HistoryEntry.default_sort.where(Sequel.like(:text, %Q{%#{params['q']}%})).all
          else
            @history = HistoryEntry.default_sort.all
          end
          slim :history, layout: :layout
        end

        app.get '/history_suggest/:histnum' do
          HistoryEntry.default_sort.first(params[:histnum].to_i).last.text
        end

        app.post '/deletehistory/:hid' do
          id = params['hid'].to_i
          bye = History::HistoryEntry.where(id: id).first
          bye.destroy
          ''
        end

      end
    end

    class HistoryEntry < Sequel::Model(DUKE_DB[:history])
      def self.save!(raw_input)
        now = DateTime.now
        og = self.find(text: raw_input)
        return og.update(recent: now, visit_count: og.visit_count + 1) if og
        self.create(text: raw_input, recent: now, orig: now, visit_count: 1)
      end

      def self.bangbang
        default_sort.first
      end

      def self.default_sort
        self.order(Sequel.desc(:recent))
      end

      def self.most_recent(limit=1)
        default_sort.limit(limit)
      end

      def ago
        ::DukeOfUrl::Formatters::Seconds.format( Time.now - recent )
      end

      def args
        words[1..-1].join(' ')
      end

      def associated_bookmark
        @associated_bookmark ||= ::DukeOfUrl::Bookmarks.bookmarked?(text).first
      end

      def associated_matcher
        @associated_matcher ||=  ::DukeOfUrl::Matchers.matched?(text)
      end

      def encoded
        ERB::Util.url_encode(text)
      end

      def export
        {
          orig: orig,
          recent: recent,
          text: text,
          visit_count: visit_count,
        }
      end

      def export_as_input
        text
      end

      def last_word
        words.last
      end

      def to_json(*a)
        text.to_json(*a)
      end

      def to_s
        text
      end

      def typeahead_match?(_)
        true
      end

      def typeahead_suggestion
        ::DukeOfUrl::TypeAheadSuggestion.new(text, '')
      end

      def words
        text.split(/\s+/)
      end
    end
  end

end
