require_relative 'handlers'

module DukeOfUrl
  VALID_URL_RE = Regexp.new('^(https{0,1}|file)://')

  module Substitutions
    def self.fetch(raw_input)
      return raw_input unless raw_input.match?(/!/)
      return History::HistoryEntry.bangbang.text if raw_input == '!!'
      return raw_input.sub('!!',History::HistoryEntry.bangbang.text) if raw_input.include?('!!')

      if /^!(\d+)/.match(raw_input)
        hist_num = $~.captures.first.to_i
        return History::HistoryEntry.most_recent(hist_num).all.last.text
      end

      if /^![?](.*)/.match(raw_input)
        needle = $~.captures.first.strip
        found = History::HistoryEntry.where(Sequel.like(:text, %Q{%#{needle}%})).last
        return '' unless found
        return found.text
      end

      # todo !-x$ etc
      if /![$]/.match?(raw_input)
        last_word = History::HistoryEntry.bangbang.last_word
        return raw_input.sub(/![$]/,last_word)
      end

      if /![*]/.match?(raw_input)
        args = History::HistoryEntry.bangbang.args
        return raw_input.sub(/![*]/,args)
      end

      return raw_input
    end
  end

  module Input

    def self.commands
      handlers.select{|handler| handler.respond_to?(:verb)}.map(&:verb)
    end

    def self.handlers
      Handlers.constants.map{|const_sym| DukeOfUrl::Input::Handlers.const_get(const_sym)}
    end

    def self.process(raw_input)
      return MultiCmd.new(raw_input) if MultiCmd.for(raw_input)

      raw_input = Substitutions.fetch(raw_input)
      return if raw_input.empty?

      notclashing = Matchers.find(raw_input)
      if notclashing != raw_input
        return notclashing if notclashing.redirect?
        return MultiCmd.new(notclashing.destination) if MultiCmd.for(notclashing.destination)
        # the alias wasn't to a website but instead to a Duke command
        raw_input = notclashing.destination
      end

      klass = handlers.find{|handler| handler.for(raw_input)} || UhOh
      print "I think #{raw_input} is a: "
      pp klass
      klass.new(raw_input)
    end


    class Parser
      include ::DukeOfUrl::Parsers::UserInput
      def initialize(raw_input)
        @raw_input = raw_input
      end
      def verb
        ''
      end
    end

    class UhOh
      def initialize(raw_input)
        @raw_input = raw_input
      end

      def as_is?
        Preferences.bool('default:destination:preserve-input')
      end

      def debug
        %Q{#{self.class.to_s.split('::').last}, default-destination set: #{pref?}, destination: #{destination}, historyText: #{as_is? ? '' : alius.verb} #{raw_input}}
      end

      def destination
        return alius.destination if pref?
      end

      def message
        return %Q{#{@raw_input} command not found} unless pref?
        ''
      end

      def persist!
        return                unless pref?
        return alius.persist! unless as_is?
        alius.uh_oh_persist!
      end

      def redirect?
        return alius.redirect? if pref?
        false
      end

      private

      def alius
        @alius ||= get_alius
      end

      def get_alius
        alius = Matchers::Alias.where(label: verb).first
        alius.raw_input = %Q{#{alius.verb} #{raw_input}}.squeeze(' ')
        alius
      end

      def pref
        @pref ||= Preferences.fetch('default:destination')
      end

      def pref?
        ! pref.empty?
      end

      def raw_input
        @raw_input
      end

      def verb
        pref
      end
    end

  end
end
