require 'rubygems'
require 'bundler'
Bundler.setup
require 'pp'
require 'date'
require 'shellwords'
require 'erb'
require 'json'
require 'yaml'

require 'sinatra'
require 'sinatra/json'
require 'tilt/sass'
require 'slim'
require 'sassc'

require 'sequel'

require_relative 'lib/db'
require_relative 'lib/junkdrawer'
require_relative 'lib/input'
require_relative 'lib/preferences'
require_relative 'lib/history'
require_relative 'lib/matchers'
require_relative 'lib/bookmarks'
require_relative 'lib/plugins'
require_relative 'lib/eximport'
require_relative 'lib/todo'
require_relative 'lib/version'

Sequel.split_symbols = true
# setting this to true will break css that uses the :empty pseudo-class
Slim::Engine.default_options[:pretty] = false

module DukeOfUrl

  TYPEAHEAD_HANDLERS = Input::Handlers.constants
                        .map{|sym| Input::Handlers.const_get(sym)}
                        .select{|const| const.is_a?(Class)}
                        .reject{|const| const.verb.empty?}

  class Web < ::Sinatra::Base
    set :bind, "0.0.0.0"
    set :public_folder, 'public'

    helpers do
      def autolink(str)
        linkable_re = /^https?:\/\//
        str.split(' ').map do |word|
          word.match?(linkable_re) ? %Q{<a href="#{word}">#{word}</a>} : word
        end.join(' ')
      end

      def prefs
        ::DukeOfUrl::Preferences
      end

      def sass(filefrag)
        file = File.read( %Q{views/sass/#{filefrag}.sass} )
        SassC::Engine.new(file, style: :compressed, syntax: :sass).render
      end

      def typeahead_handlers(raw_input)
        TYPEAHEAD_HANDLERS.map{|handler| handler.new(raw_input)}
      end

    end

    post '/typeahead' do
      request.body.rewind
      user         = JSON.parse(request.body.read)
      raw_input    = user['input']

      candidates = [
        Bookmarks::Bookmark.where( Sequel.like(:url, %Q{%#{raw_input}%} )).all,
        History::HistoryEntry.where( Sequel.like(:text, %Q{%#{raw_input}%} )).all,
        typeahead_handlers(raw_input),
        Matchers.typeahead,
        Preferences.typeahead(raw_input),
      ].flatten

      shortest_straws = candidates.find_all{|thing| thing.typeahead_match?(raw_input) }
      shortest_straws = shortest_straws.map(&:typeahead_suggestion).flatten.sort.uniq

      json shortest_straws# pulled for YOU
    end

    get '/css/*.css' do
      content_type 'text/css', :charset => 'utf-8'
      sass params[:splat].first
    end

    get '/' do
      histcount = prefs.int('history:show')
      @history =  DukeOfUrl::History.read(histcount)

      if params["q"] && ! params["q"].strip.empty?
        @handler = Input.process(params['q'])
        unless @handler.nil?
          @handler.persist!
          redirect @handler.destination if @handler.redirect?
        end
      end

      slim :index, layout: :layout
    end

    register Bookmarks::Routes
    register History::Routes
    register Preferences::Routes
    register Export::Routes
    register Matchers::Routes
    register ToDo::Routes

    Plugins.route_modules.each{|route_module| register route_module}

    get '/opensearch.xml' do
      @duke = "#{request.scheme}://#{request.host}:#{request.port}"
      headers "Content-Type" => "application/opensearchdescription+xml"
      slim :opensearch, layout: false
    end

  end
end
