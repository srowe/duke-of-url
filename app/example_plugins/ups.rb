module UnitedPS
  module ::DukeOfUrl::Input
    module Handlers
      class UpsTracker < AbstractHandler

        def self.for(input)
          input.length == 18 && input.start_with?('1Z')
        end

        def destination
          "https://www.ups.com/track?loc=en_US&requester=ST&trackNums=#{raw_input}/trackdetails"
        end

        private

        def history?
          true
        end

      end
    end
  end
end
